import math
import random
import socket

"""
liad's rsa implementation
why ? : for learning the encryption
 """
 
 
IP = "0.0.0.0"
PORT = 8000

#greater common divisor
def gcd(a, b):
    while b != 0:
        a, b = b, a % b
    return a
	

def is_prime(n):
    if n % 2 == 0 and n > 2: 
        return False
    for i in range(3, int(math.sqrt(n)) + 1, 2):
        if n % i == 0:
            return False
    return True

#Choose closest numbers to the beginning range
def get_pq():
	start = random.randint(5,500)
	while True:
		if is_prime(start):
			return start
		start = start + 1
		
		

#return list of n and z
def get_nz():
	p = get_pq()
	q = get_pq()
	while p == q:
		q = get_pq()
	return [p*q, (q-1) * (p-1)]

#random e number
def get_e(z):
	e = random.randrange(2, z)
	g = gcd(e, z)
	while g != 1:
		e = random.randrange(1, z)
		g = gcd(e, z)
	return e

		
def get_d(z, e):
	"""(ed-1) %n = 0 """
	d = 0
	x1 = 0
	x2 = 1
	y1 = 1
	temp_z = z
	while e > 0:
		temp1 = temp_z / e
		temp2 = temp_z - temp1 * e
		temp_z = e
		e = temp2

		x = x2- temp1* x1
		y = d - temp1 * y1

		x2 = x1
		x1 = x
		d = y1
		y1 = y

	if temp_z == 1:
		return d + z
	
	
#Each letter
def encrypt(n, e, text):
	cipher = [(ord(c) ** e) % n for c in text]
	return cipher

#Each letter
def decrypt(d, n, text):
	plaintext= [chr((int(st) ** d) % n) for st in text]
	return plaintext
	
	
	
def createServer(e, d, n):
	s = socket.socket()
	s.bind((IP, PORT))
	s.listen(1)
	cs , ca = s.accept()
	try:
		while True:
			data = cs.recv(1024)
			if data == "e and n":
				cs.send(str(e) + " " + str(n))
			elif len(data)>1:
				lst = data.split(',')
				data = decrypt(d, n, lst)
				
				print 'got: ' + str(lst)
				print 'user sent: ' + ''.join(data)
				
	except Exception as inst:
		print inst
		cs.close()
		s.close()

def main():			
	n,z = get_nz()
	e = get_e(z)
	d = get_d(z, e)
	print 'n:' + str(n)
	print 'e:' + str(e)
	print 'd:' + str(d)
	createServer(e, d, n)

if __name__ == "__main__":
	main()
