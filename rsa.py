import math
import random
"""
liad's rsa implementation
why ? : for learning the encryption
 """
#find the max same divider
def gcd(a, b):
    while b != 0:
        a, b = b, a % b
    return a
	
#the name:)

def is_prime(n):
    if n % 2 == 0 and n > 2: 
        return False
    for i in range(3, int(math.sqrt(n)) + 1, 2):
        if n % i == 0:
            return False
    return True

#Choose closest numbers to the beginning range	
def get_pq():
	start = 1
	while start <= 5:
		start = int(raw_input("please enter number to start from"))
	while True:
		if is_prime(start):
			return start
		start = start + 1
		
		

#return list of n and z
def get_nz():
	p = get_pq()
	q = get_pq()
	while p == q:
		q = get_pq()
	return [p*q,(q-1)*(p-1)]

#random e number
def get_e(z):
	e = random.randrange(2, z)
	g = gcd(e, z)
	while g != 1:
		e = random.randrange(1, z)
		g = gcd(e, z)
	return e

		
def get_d(z,e):
	"""ed-1 %n = 0 """
	d = 0
	x1 = 0
	x2 = 1
	y1 = 1
	temp_z = z

	while e > 0:
		temp1 = temp_z/e
		temp2 = temp_z - temp1 * e
		temp_z = e
		e = temp2

		x = x2- temp1* x1
		y = d - temp1 * y1

		x2 = x1
		x1 = x
		d = y1
		y1 = y

	if temp_z == 1:
		return d + z
	
	
#Each letter
def encrypt(n,e,text):
	    
	cipher = [(ord(c) ** e) % n for c in text]
	return cipher

#Each letter
def decrypt(d,n,text):
	plaintext= [chr((char ** d) % n) for char in text]
	return plaintext
	
text = raw_input("please enter text to encrypt:")
print 'text:'+text
n,z = get_nz()
print "n:"+str(n)+":"+"z:"+str(z)
e = get_e(z)
print "e:"+str(e)
d = get_d(z,e)
print "d:"+str(d)
text =  encrypt(n,e,text)
print "text:"+str(text) 
text = decrypt(d,n,text)
print "after: "+"".join(text)